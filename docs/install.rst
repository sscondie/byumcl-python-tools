.. _install:

.. currentmodule:: byumcl

************
Installation
************

Installing the byumcl library is the same on all platforms. The source
code is hosted at https://bitbucket.org/byumcl/byumcl and

::

  git clone https://spencerlyon2@bitbucket.org/byumcl/byumcl.git
  cd byumcl
  python setup.py install



Dependencies
~~~~~~~~~~~~

  * `NumPy <http://www.numpy.org>`__: 1.6.1 or higher
  * `SciPy <http://www.scipy.org>`__: miscellaneous statistical functions
  * `pandas <http://pandas.pydata.org/>`__: data handling and io tools
  * `Cython <http://www.cython.org>`__: needed to build optional Cython
    extensions

Test
~~~~
.. automodule:: byumcl.interp.interp
   :members:

.. automodule:: byumcl.matstat.normal
    :members:
