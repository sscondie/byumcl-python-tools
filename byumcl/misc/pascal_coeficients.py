"""
Created July 13, 2012

Author: Spencer Lyon

Find the number of coefficients needed in a standard polynomial
expansion in a specified number of dimensions and specified order.
"""

import numpy as np

__all__ = ['poly_ncoefs']


def pascal(n):
    """
    Yield up to row ``n`` of Pascal's triangle, one row at a time.

    The first row is row 0.
    """
    def newrow(row):
        "Calculate a row of Pascal's triangle given the previous one."
        prev = 0
        for x in row:
            yield prev + x
            prev = x
        yield 1

    prevrow = [1]
    yield prevrow
    for x in xrange(n):
        prevrow = list(newrow(prevrow))
        yield prevrow


def poly_ncoefs(h, j):
    """
    Compute the number of coefficients in a ``h``-dimensional ``j``
    order polynomial expansion.

    Parameters
    ----------
    h: number, int
        The number of parameters in the polynomial expansion.
    j: number, int
        The degree of the expansion.

    Returns
    -------
    element: number, int
        The number of coefficients in a jth order polynomial expansion in terms
        of h parameters.
    """
    # gather all rows into a list
    pas = np.asarray(list(pascal(h + j)))

    #Pull out the last row
    theRow = pas[-1]

    # grab the correct element.
    element = theRow[j]

    return element
