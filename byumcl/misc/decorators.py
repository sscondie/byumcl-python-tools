"""
Created Apr 2, 2013

Author: Spencer Lyon

Module that holds various decorators to achieve random things

TODOS:
    - use the decorator package from PyPy to have these preserve
        docstrings and function signatures.
    - Add decorators for making matplotlib plots interactive. Use
        buttons and sliders to start, then move to fancier things.
    - Look at using some things from profilehooks. This will be handy
        for profiling/coverage testing individual functions in a larger
        program
"""
import time
import collections
import functools
import warnings
import numpy as np


def timer(label='', trace=True):
    """
    Decorator that will track the time spent on a particular function
    call as well as the total time spent in that function since python
    started.

    Parameters
    ----------
    label : str, optional(default='')
        An optional label to be appended to each print out of the time
        needed for the function to complete.

    trace : bool, optional(default=True)
        Whether or not the decorator should trace and report on each
        call to the function

    References
    ----------
    This decorator was taken from
        Learning Python 4th Edition by Mark Lutz

    Notes
    -----
    Also, to access cumulative time spent in a function, the user will
    need to call func_name.alltime (data lives in all time parameter)
    """
    def onDecorator(func):  # on decorator args: keep args
        def onCall(*args, **kargs):  # on @: keep decorated func
            start = time.clock()
            result = func(*args, **kargs)  # on call: use original
            elapsed = time.clock() - start
            onCall.alltime += elapsed  # give this function the alltime attr
            if trace:
                format = '%s%s: %.5f, %.5f'
                values = (label, func.__name__, elapsed, onCall.alltime)
                print(format % values)
            return result
        onCall.alltime = 0
        return onCall
    return onDecorator


class singleton(object):
    """
    Decorator that enforces the singleton design pattern. This means
    that there is only ever one instance of a particular class in
    existence at any given time. Any attempt to create a second one will
    simply return the existing instance.

    References
    ----------
    This decorator was taken from
        Learning Python 4th Edition by Mark Lutz
    """
    def __init__(self, aClass):
        self.aClass = aClass
        self.instance = None

    def __call__(self, *args, **kwargs):
        if self.instance == None:
            self.instance = self.aClass(*args, **kwargs)
        return self.instance


class memoized(object):
    """
    Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is
    returned (not reevaluated). This should be a good time save when
    doing calculations that take a long time and might have repetitive
    arguments.

    References
    ----------
    This decorator was taken from
        http://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    """
    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        if not isinstance(args, collections.Hashable):
            # uncacheable. a list, for instance.
            # better to not cache than blow up.
            return self.func(*args)

        if args in self.cache:
            return self.cache[args]

        else:
            value = self.func(*args)
            self.cache[args] = value
            return value

        def __repr__(self):
            return self.func.__doc__  # Return the function's docstring.

        def __get__(self, obj, objtype):
            # Support instance methods.
            return functools.partial(self.__call__, obj)


def deprecated(func):
    """
    This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.

    Examples
    --------
    @deprecated
    def my_func():
        pass

    @other_decorators_must_be_upper
    @deprecated
    def my_func():
        pass

    References
    ----------
    This decorator was taken from
        http://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    """

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.warn_explicit(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning,
            filename=func.func_code.co_filename,
            lineno=func.func_code.co_firstlineno + 1
        )
        return func(*args, **kwargs)
    return new_func

#--------------------Decorators written by Roy Roth--------------------------#
# author: Roy Roth (roykroth@gmail.com)
# description: Defines 2 decorators. The first allows the user to append to
# the doc string of the decorated function, and the second aims to replicate
# some of the functionality of Ipython's timeit.
def append_to_docstring(to_add):
    '''
    Decorator Factory producing a decorator appending to a functions
    doc string. This is useful when creating other decorators that
    modify the behavior of the functions they decorate

    Parameters
    ----------
    to_add: str 
        String to add to the doc string of decorated function
    '''
    def _append(func):
        if not func.__doc__:
            func.__doc__ = to_add
        else:
            func.__doc__ += '\n' + to_add
        return func
    return _append

def timeit(n = 10):
    '''
    Decorator Factory producing a decorator the runs the decorated
    funtion n times and reports the average execution time, as well 
    as returning the return value of the  function being timed 
    (The return value for the timed function is the value from the 
    last time the funtion is called)
   
    Parameters
    ----------
    n: int, optional (default = 10)
        The number of times to run the function.
    '''
    def _timer(func): # This is the actual decorator
        add = ('Decorated Function Returns\n' + 
               '--------------------------\n' + 
               ("{Time: average execution time,\n" +
                "Value: Undecorated Function's return value}"))
        @append_to_docstring(add)
        @functools.wraps(func) #Preserve decorated function's documentation
        def inner(*args, **kwargs):
            time_list = []
            for i in xrange(n):
                t1 = time.time()
                val = func(*args, **kwargs)
                elapsed = time.time() - t1
                time_list.append(elapsed)
            avg_time = np.array(time_list).mean()
            ret = {'Time': avg_time, 'Value': val}
            return ret
        return inner
    return _timer
