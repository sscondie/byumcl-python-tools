#in_dev.py
"""Contains code in development.

Migrating developmental code here.
"""

def meshgrid(X, Y):
    """Returns coordnitate matrix for two coordinate vectors.

    Like the numpy version, meshgrid make coordinate arrays for vectorized
    evaluations of scalar/vector fields over grids, given one-dimensional
    coordinate arrays x1 and x2. However, this only works for two vectors.

    Example:
    >>> nx, ny = (3, 2)
    >>> x = np.linspace(0, 1, nx)
    >>> y = np.linspace(0, 1, ny)
    >>> xv, yv = meshgrid(x, y)
    >>> xv
    array([[ 0. ,  0.5,  1. ],
        [ 0. ,  0.5,  1. ]])
    >>> yv
    array([[ 0.,  0.,  0.],
        [ 1.,  1.,  1.]])
    """
    pass
