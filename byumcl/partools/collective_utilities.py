#collective_utilities.py
"""Collective utilities to aid programming in distributed memory environment.

This module contains utilities that are collective in nature (they take in an
MPI communicator as an argument) and are desigen to aid working in the parallel
environment.

By Jeremy Bejarano
"""

try:
    from mpi4py import MPI
except:
    print('Cannot find mpi4py. partools will not work')

from array import array



def rprint(comm, message):
    """rprint(comm, message)

    Prints only the message on the root.

    Root print. This is a convenience function that prints only the function on the root
    process.

    Example:,

    For a comm with size 3,

    rank = comm.rank
    rprint(comm, "Hello")

    results in

    "Hello"

    printed only from the root node.
    """

    if comm.rank == 0:
        print message



def gather_strings(comm, message):
    """gather_strings(comm, message)

    Gather messages (strings) from each process and return as a list.

    Note that each process will be returned a list. The list will be empty on
    all but the root process.
    """

    new_comm = comm.Split()

    size = new_comm.size
    rank = new_comm.rank

    gathered_message = []

    #This should optimally be done with collective communication, however,
    #I have had trouble making with anything but the current method
    if rank == 0:
        gathered_message.append(message)
        for proc in range(1, size):
            buf = array('c', '\0') * 256
            r = comm.Irecv(buf, source=proc)
            status = MPI.Status()
            r.Wait(status)
            n = status.Get_count(MPI.CHAR)
            recv_message = buf[:n].tostring()
            gathered_message.append(recv_message)
    else:
        comm.Send(message, dest=0)

    return gathered_message


def par_print(comm, message):
    """par_print(comm, message)

    Print a message sent from local processes on the root process.

    This is a blocking call that gathers "local_message" from each process
    and prints from the root process. It can be used a debug tool or a parallel
    "print" tool.

    Example:

    For comm with size=4

    rank = comm.Get_rank()
    message = "Hello from process " + str(rank)
    par_print(comm, rank)

    results in

    [0]: Hello from process 0
    [1]: Hello from process 1
    [2]: Hello from process 2
    [3]: Hello from process 3

    printed from the root node only.
    """

    gathered_strings = gather_strings(comm, message)
    
    if comm.rank == 0:

        for proc, message in enumerate(gathered_strings):
            print "[%d]: " % proc + message









